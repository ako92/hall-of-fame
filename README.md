# Hall Of Fame

## Introduction

>This web application is a test work for Pixels and code company.  
>All you have to do is install dependencies using:
>`npm install`
 and run it using:
>`npm start`

## Technologies and libraries used
React using Create-React-App, Redux, node-sass, rc-pagination, react-hooks

