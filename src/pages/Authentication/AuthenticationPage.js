import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Login } from '../../store/authentication/action/action';
import { createSelector } from 'reselect';
import AuthorizeApp from '../../functionalities/AuthenticatedRoute/authorize-app/AuthorizeApp';
import { useHistory } from 'react-router';

function AuthenticationPage() {
    const [password, setPassword] = useState("")
    const [username, setUsername] = useState("")
    const dispatch = useDispatch()
    const loginSelector = createSelector((state) => state.loginReducer, state => state.loginReducer, (loginReducer) => loginReducer)
    const loginReducer = useSelector(loginSelector)
    const history = useHistory();
    const [loginError, setLoginError] = useState()

    useEffect(() => {
        let authorized = AuthorizeApp()

        if (authorized && !loginReducer.authenticated) {
            // if we are authorized based on localStorage
            history.replace("/fames")
        }
        else if (loginReducer.authenticated && loginReducer.status.data.data.success) {
            // if we logged in succesfully
            localStorage.setItem("JWT", loginReducer.status.headers.authorization)
            AuthorizeApp(loginReducer.status.headers.authorization)
            history.replace("/fames")
        }
        else if (!authorized && loginReducer.authenticated && !loginReducer.status.data.data.success) {
            // if we had incorrect userName or password or login wasnt succesfull
            setLoginError("username or password is incorrect")

        }
    }, [loginReducer])


    return (

        <div className="login-page">
            <div className="form">


                <form className="login-form">
                    <input type="text" onFocus={() => { setLoginError("") }} placeholder="username" onChange={(e) =>
                        setUsername(e.target.value)

                    } />
                    <input type="password"  onFocus={() => { setLoginError("") }} placeholder="password" onChange={(e) =>
                        setPassword(e.target.value)
                    } />
                    <button onClick={(e) => {
                        e.preventDefault()
                        const form = new FormData()
                        form.append("username", username)
                        form.append("password", password)
                        dispatch(Login(form))

                    }}>login</button>
                </form>
                {loginError && loginError !== "" && <span className="login-error">{loginError}</span>}

            </div>
        </div>

    )


}
export default AuthenticationPage;