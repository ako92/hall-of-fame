import React, { useEffect, useState } from 'react'
import { tokenContext } from '../../functionalities/AuthenticatedRoute/authorize-app/AuthorizeApp'
import { useDispatch, useSelector } from 'react-redux'
import { GetFames } from '../../store/fames/action/action'
import FameCard from '../../components/FameCard/FameCard'
import { createSelector } from 'reselect'
import Pagination from 'rc-pagination'
import "rc-pagination/assets/index.css"
import NavBar from '../../components/NavBar/NavBar'

function Fames() {
    const dispatch = useDispatch()
    const famesSelector = createSelector((state) => state.famesReducer, state => state.famesReducer, (famesReducer) => famesReducer)
    const famesReducer = useSelector(famesSelector)
    const [currentPage, setCurrentPage] = useState(1)
    useEffect(() => {
        if (tokenContext._currentValue) {
            dispatch(GetFames(tokenContext._currentValue, currentPage === 1 ? 0 : 1))
        }
    }, [currentPage])

    
    const pageChanger = (pageNumber) => {
        setCurrentPage(pageNumber)

    }
    if (famesReducer.fetched) {
        const { list, size } = famesReducer.status
        return (
            <React.Fragment>
                <NavBar />

                <div className="container">

                    <div className="row pt-3 ">
                        {list.map(item =>
                            <div key={item.id} className="col-md-3 col-sm-12 pb-3 ml-4              ">
                                <FameCard key={item.id} fame={item} />
                            </div>
                        )}
                    </div>
                    <div className="center">
                        <Pagination pagSize={size} onChange={pageChanger} current={currentPage} total={16} />
                    </div>
                </div>

            </React.Fragment>
        )
    }
    else {
        return (
            <span>
                please wait.
            </span>
        )
    }


}
export default Fames