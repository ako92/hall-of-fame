import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import AuthorizeApp from './authorize-app/AuthorizeApp';


function AuthenticatedRoute({ render: Render, component: Component, ...rest }) {

    let authorized = AuthorizeApp();
    // if we are logged in the authorized variable is true. 
    return (
        <Route {...rest}
            render={props => {
                // if we are not authorized
                if (!authorized) {
                    return (<Redirect to="/" />)
                }
                else {
                    // if we are authorized
                    if (Render) {
                        // if we had match.params pattern
                        return (
                            <Render {...props}></Render>
                        )
                    }
                    else {
                        // if had simple route that dont have any param
                        return (
                            <Component {...props} />
                        )
                    }
                }

            }}
        />

    )
}
export default AuthenticatedRoute;