import React from 'react'


export let tokenContext


export default function AuthorizeApp(JWT) {
    // this module will handle authorization in two different model
    // 1. if we recently logged in and if there'a token i localStorage
    // 2. if we logged in now.
    // in both cases it will return that we are authorized or not.


    // in this module we have to verify token too. but there's no api for that.
    

    let authorized = false
    if (JWT === undefined && localStorage.getItem("JWT")) {
        tokenContext = React.createContext(localStorage.getItem("JWT"))
        authorized = true
    }
    else if (JWT) {
        tokenContext = React.createContext(JWT)
        authorized = true
    }
    return authorized


}