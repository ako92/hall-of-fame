import React from 'react';
import { Switch, Route } from 'react-router';
import { BrowserRouter as Router } from 'react-router-dom';
import AuthenticationPage from './pages/Authentication/AuthenticationPage';
import AuthenticatedRoute from './functionalities/AuthenticatedRoute/AuthenticatedRoute';
import Details from './pages/Details/Details';
import Fames from './pages/Fames/Fames';
import "bootstrap/dist/css/bootstrap.min.css"
import './styles/styles.scss'
import rootReducer from './store';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import Promise from "redux-promise-middleware";
import logger from "redux-logger";
import thunk from "redux-thunk";

const createStoreWithMiddleware = applyMiddleware(Promise, thunk, logger)(createStore);
const store = createStoreWithMiddleware(rootReducer);

function App() {

  return (
    
<Provider store={store}>
      <Router>
        <Switch>
          <Route exact component={AuthenticationPage} path="/" />
          <AuthenticatedRoute exact path="/fames" component={Fames} />
          <AuthenticatedRoute exact path="/fames/:id" render={({ match }) => <Details id={match.params.id} />} />
        </Switch>
      </Router>

      </Provider>
  );
}

export default App;
