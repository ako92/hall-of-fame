

import { combineReducers } from "redux";
import { LoginReducer, LogoutReducer } from "./authentication/reducer/reducer";
import { FamesReducer } from "./fames/reducer/reducer";


const rootReducer = combineReducers({
    loginReducer: LoginReducer,
    famesReducer: FamesReducer,
    logoutReducer: LogoutReducer
});


export default rootReducer;
