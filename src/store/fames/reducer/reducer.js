export const FETCH_FAMES_SUCCESFULL = "fetch_fames_succesfull"
export const FETCH_FAMES_ERROR = "fetch_fames_error"



export function FamesReducer(
    state = {
        status: {},
        fetched: false,
        error: null
    },
    action) {
    switch (action.type) {
        case FETCH_FAMES_SUCCESFULL: {
            return {
                ...state,
                fetched: true,
                status: action.payload
            };
        }
        case FETCH_FAMES_ERROR: {
            return {
                ...state,
                fetched: false,
                error: action.payload
            };
        }
        default:
            return state;
    }

}
