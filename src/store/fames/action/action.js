
import axios from "axios";
import { API_ENDPOINT } from "../../../env";
import { FETCH_FAMES_ERROR, FETCH_FAMES_SUCCESFULL } from "../reducer/reducer";


export function GetFames(token, pageNumber) {
    return function (dispatch) {
        axios
            .get(`${API_ENDPOINT}/fames?page=${pageNumber}`, {
                headers: {
                    authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                dispatch({
                    type: FETCH_FAMES_SUCCESFULL,
                    payload: response.data.data
                });
            })
            .catch(err => {
                dispatch({
                    type: FETCH_FAMES_ERROR,
                    payload: err
                });
            });
    };
}


