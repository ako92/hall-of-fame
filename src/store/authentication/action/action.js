
import axios from "axios";
import { API_ENDPOINT } from "../../../env";
import { LOGIN_SUCCESFULL, LOGIN_ERROR, LOGGED_OUT_SUCCESFULL, LOG_OUT_ERROR } from "../reducer/reducer";


export function Login(loginCredentials) {
  return function (dispatch) {
    axios
      .post(`${API_ENDPOINT}/login`, loginCredentials)
      .then(response => {
        dispatch({
          type: LOGIN_SUCCESFULL,
          payload: response
        });
      })
      .catch(err => {
        dispatch({
          type: LOGIN_ERROR,
          payload: err
        });
      });
  };
}


export function Logout(token) {
  console.log("token", token)
  let formData = new FormData()
  return function (dispatch) {
    axios
      .post(`${API_ENDPOINT}/logout`, formData, {
        headers: {
          "Authorization": `Bearer ${token}`
        }
      })
      .then(response => {
        dispatch({
          type: LOGGED_OUT_SUCCESFULL,
          payload: response
        });
      })
      .catch(err => {
        dispatch({
          type: LOG_OUT_ERROR,
          payload: err
        });
      });
  };
}
