export const LOGIN_SUCCESFULL = "login_succesfull"
export const LOGIN_ERROR = "login_error"

export const LOGGED_OUT_SUCCESFULL = "logged_out_succesfull"
export const LOG_OUT_ERROR = "log_out_error"


export function LoginReducer(
    state = {
        status: {},
        authenticated: false,
        error: null
    },
    action) {
    switch (action.type) {
        case LOGIN_SUCCESFULL: {
            return {
                ...state,
                authenticated: true,
                status: action.payload
            };
        }
        case LOGIN_ERROR: {
            return {
                ...state,
                authenticated: false,
                error: action.payload
            };
        }
        default:
            return state;
    }

}



export function LogoutReducer(
    state = {
        status: {},
        loggedOut: false,
        error: null
    },
    action) {
    switch (action.type) {
        case LOGGED_OUT_SUCCESFULL: {
            return {
                ...state,
                loggedOut: true,
                status: action.payload
            };
        }
        case LOG_OUT_ERROR: {
            return {
                ...state,
                loggedOut: false,
                error: action.payload
            };
        }
        default:
            return state;
    }

}
