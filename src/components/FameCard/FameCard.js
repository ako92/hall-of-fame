import React from "react"
import { Link } from 'react-router-dom'

export default function FameCard(Props) {
    console.log(Props.fame)
    return (
        <Link className="fame-card" to={`/fames/${Props.fame.id}`}>

            
            <img class="card-img-top" alt={Props.fame.name} src={Props.fame.image} />
            <div className="card-body">
                <div>
                    <p className="card-title">
                        {Props.fame.name}
                    </p>
                </div>
                <p className="card-text">
                    {Props.fame.dob}
                </p>
            </div>
        </Link>
    )
}



