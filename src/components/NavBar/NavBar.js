import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import { Logout } from '../../store/authentication/action/action'
import { tokenContext } from '../../functionalities/AuthenticatedRoute/authorize-app/AuthorizeApp'




export default function NavBar() {

    const dispatch = useDispatch()
    const logoutSelector = createSelector((state) => state.logoutReducer, state => state.logoutReducer, (logoutReducer) => logoutReducer)
    const logoutReducer = useSelector(logoutSelector)


    useEffect(() => {

        if (logoutReducer.loggedOut) {
            // on logout token from localestorage will remove
            localStorage.removeItem("JWT")
            window.location.reload()

        }
    }, [logoutReducer])


    return (
        <nav className="navbar float-right navbar-expand-sm bg-light navbar-light">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <button className="navbutton" onClick={() => {

                        dispatch(Logout(tokenContext._currentValue))
                    }}>Logout</button>
                </li>

            </ul>
        </nav>
    )
}